from dataclasses import dataclass
from os import environ

from requests import get, post

class bnet_api:

    class bnet_locales:
        US = 'en_US'
        MX = 'es_MX'
        BR = 'pt_BR'
        GB = 'en_GB'
        ES = 'es_ES'
        FR = 'fr_FR'
        RU = 'ru_RU'
        DE = 'de_DE'
        PT = 'pt_PT'
        IT = 'it_IT'

    class bnet_regions:
        US = 'us'
        EU = 'eu'
        APAC = 'apac'

    class bnet_namespace:
        US_STATIC   = 'static-us'
        US_DYNAMIC  = 'dynamic-us'
        # TODO: complete namespace entries

    class endpoints:
        base = 'https://us.api.blizzard.com'
        class token:
            fetch = 'https://oauth.battle.net/token'
        class spells:
            get_by_id = '/data/wow/spell/%s'

    @dataclass
    class spell:
        id: str
        name: str
        description: str
        # TODO: retrieve media automatically in get_spell_by_id
        media: str

    def __init__(self, region: bnet_regions, locale: bnet_locales, namespace: bnet_namespace, id: str, key: str ):
        self.region = region
        self.locale = locale
        self.namespace = namespace
        # TODO: handle token age, store token to env( ? )
        self.token = self.__fetch_token__( id, key )
        self.headers = {
            'Authorization': f"Bearer { self.token }"
        }

    def __fetch_token__( self, id: str, key: str ):
        token_response = post( self.endpoints.token.fetch, auth=( id, key ), data={ 'grant_type': 'client_credentials' } ).json()
        return token_response[ 'access_token' ]

    def __get__( self, url: str ):
        url = f"{ url }?namespace={ self.namespace }&locale={ self.locale }"
        print( url )
        return get( url, headers=self.headers ).json()

    def get_spell_by_id( self, id: int ):
        endpoint = self.endpoints.spells.get_by_id % id
        url = f"{ self.endpoints.base }{ endpoint }"
        rjson = self.__get__( url )
        spell = self.spell( id=rjson['id'], name=rjson['name'], description=rjson['description'], media=rjson[ 'media' ][ 'key' ][ 'href' ] )
        return spell
